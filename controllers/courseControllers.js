const mongoose = require("mongoose");
const Course = require("../models/course.js");

// 2. Modify the addcourse controller method to implement admin authentication.

// module.exports.addCourse = (reqBody) => {
//     let newCourse = new Course(
//         {
//             name: reqBody.name,
//             description: reqBody.description,
//             price: reqBody.price
//         }
//     )

//     return newCourse.save().then((newCourse, error) => {
//         if(error){
//             return error;
//         } else {
//             return newCourse;
//         }
//     })
// }

module.exports.addCourse = (reqBody, authData) => {
    if (authData.isAdmin == true) {
        let newCourse = new Course(
            {
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price,
				slots: reqBody.slots
            }
        )
    
        return newCourse.save().then((newCourse, error) => {
            if(error){
                return error;
            } else {
                return newCourse;
            }
        })
    } else {
        let message = Promise.resolve("User must be ADMIN to add courses");
        return message.then((value) => {return value});
    }
}

module.exports.getAllCourse = () => {
    return Course.find({}).then(result => {
        return result;
    })
}

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

module.exports.archiveCourse = (courseId, authData) => {
	if(authData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: false
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			// return true
            return `Course ${result.name} is now archived.`
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}
