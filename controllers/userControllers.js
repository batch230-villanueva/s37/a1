const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/course.js")

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
		// hashing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return newUser;
		}
	})
}

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(
        result => {
            if (result.length > 0) {
                return true;
            } else {
                return false;
            }
        }
    )
}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(
        result => {
            if (result == null) {
                return "Email not Found";
            } else {
                const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

                if(isPasswordCorrect) {
                    return {access: auth.createAccessToken(result)};
                } else {
                    return "Password Incorrect";
                }
            }

            
        }
    )
}

module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody._id).then(
        (result, err) => {
            if(err){
                return false;
            } else {
                result.password = "";
                return result;
            }
        }
    )
}

// S41

// module.exports.getProfile = (request, response) => {
//     const userData = auth.decode()
// }

// enroll feature
module.exports.enroll = async (request, response) => {
    const userData = auth.decode(request.headers.authorization)

    let courseName = await Course.findById(request.body.courseId).then(result => result.name);

    let newData = {
        userId: userData.id,
        email: userData.email,
        courseId: request.body.courseId,
        courseName: courseName
    }

    console.log(newData)

    let isUserUpdated = await User.findById(newData.userId)
    .then(user => {
            user.enrollments.push({
                courseId: newData.courseId,
                courseName: newData.courseName
            })

            return user.save()
            .then(result => {
                console.log(result)
                return true;
            })
            .catch(error => {
                console.log(error);
                return false;
            })
        })
 
    console.log(isUserUpdated)

    let isCourseUpdated = await Course.findById(newData.courseId)
    .then(course => {
        course.enrollees.push({
            userId: newData.userId,
            email: newData.email
        })

        switch(true) {
            case course.slots === 0:
                response.send("Enrollment failed. No more slots available");
                break;
            
            case course.slots < 0:
                course.slots = 0;
                break;
            
            default:
                course.slots -= 1;
                return course.save();
                break;     
        }

        // course.slots -= 1;

        // return course.save();
    }).then(result => {
        console.log(result);
        return true;
    }).catch(error => {
        console.log(error);
        return false;
    })

    console.log(isCourseUpdated);


    (isUserUpdated && isCourseUpdated) ? response.send(true) : response.send(false);

} 