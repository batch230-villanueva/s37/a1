const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Activity 
// 1. Refactor the "course" route to implement user authentication for the admin when creating a course

// router.post("/create", (request, response) => {
//     courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController));
// })

router.post("/create", auth.verify, (request, response) => {
    const authData = {
        isAdmin : auth.decode(request.headers.authorization).isAdmin
    }

    courseController.addCourse(request.body, authData).then(resultFromController => response.send(resultFromController));
})

router.get("/getAll", (request,response) => {
    courseController.getAllCourse().then(
        resultFromController => response.send(resultFromController)
    )
})

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseController.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const authData = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseController.archiveCourse(request.params.courseId, authData).then(resultFromController => {
		response.send(resultFromController)
	})
})


module.exports = router;